import React, { Component } from "react";
import { Control, LocalForm, Errors } from "react-redux-form";
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  Breadcrumb,
  BreadcrumbItem,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Row,
  Col,
  Label,
} from "reactstrap";
import { Link } from "react-router-dom";
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';
import { FadeTransform, Fade, Stagger } from 'react-animation-components';

function RenderDish({ dish }) {
  if (dish != null)
    return (
      <Card>
           <CardImg top src={baseUrl + dish.image} alt={dish.name} />
        <CardBody>
          <CardTitle>{dish.name}</CardTitle>
          <CardText>{dish.description}</CardText>
        </CardBody>
      </Card>
    );
  else return <div></div>;
}

function RenderComments({ comment, postComment, dishId }) {
  if (comment != null) {
    console.log(comment);
    const listComments = comment.map((comment) => {
      var isoDate = new Date(comment.date);
      var date =
        isoDate.toLocaleString("default", { month: "long" }) +
        " " +
        isoDate.getUTCDate() +
        ", " +
        isoDate.getUTCFullYear();
      return (
        <div>
          <Fade in>
          <div className="row">
            <div className="col-12 my-2">{comment.comment}</div>
            <div className="col-12 my-2">
              -- {comment.author} , {date}
            </div>
          </div>
          </Fade>
        </div>
      );
    });

    return (
      <div>
        <h4>Comments</h4>
        <Stagger in>
        {listComments}
        <CommentForm dishId={dishId} postComment={postComment} />
        </Stagger>
                        
      </div>
    );
  } else return <div></div>;
}
const DishDetail = (props) => {
    if (props.isLoading) {
        return(
            <div className="container">
                <div className="row">            
                    <Loading />
                </div>
            </div>
        );
    }
    else if (props.errMess) {
        return(
            <div className="container">
                <div className="row">            
                    <h4>{props.errMess}</h4>
                </div>
            </div>
        );
    }
    else if (props.dish != null) 
  return (
    <div className="container">
      <div className="row">
        <Breadcrumb>
          <BreadcrumbItem>
            <Link to="/menu">Menu</Link>
          </BreadcrumbItem>
          <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
        </Breadcrumb>
        <div className="col-12">
          <h3>{props.dish.name}</h3>
          <hr />
        </div>
      </div>
      <div className="row">
        <div className="col-12 col-md-5 m-1 text-left">
          <RenderDish dish={props.dish} />
          {/* {this.renderDish(this.props.dish)} */}
        </div>
        <div className="col-12 col-md-5 m-1 text-left">
          <RenderComments
            comment={props.comments}
            postComment={props.postComment}
            dishId={props.dish.id}
          />
          {/* {this.renderComment(this.props.comments,this.props.addComment,this.props.dish.id)} */}
        </div>
      </div>
    </div>
  );
};

export default DishDetail;
const required = (val) => val && val.length;
const maxLength = (len) => (val) => !val || val.length <= len;
const minLength = (len) => (val) => val && val.length >= len;

export class CommentForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isModalOpen: false,
    };
    this.toggleModal = this.toggleModal.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  toggleModal() {
    this.setState({
      isModalOpen: !this.state.isModalOpen,
    });
  }

  handleSubmit(values) {
    this.toggleModal();
    this.props.postComment(this.props.dishId, values.rating, values.author, values.comment);
  }

  render() {
    return (
      <div>
        <Button outline color="secondary" onClick={this.toggleModal}>
          <i className="fa fa-pencil mr-2"></i>Submit Comment
        </Button>
        <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
          <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
          <ModalBody>
            <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
              <Row className="form-group">
                <Col htmlFor="number" md={12} className="mb-2">
                  Rating
                </Col>
                <Col md={12}>
                  <Control.select
                    model=".number"
                    id="number"
                    name="number"
                    className="form-control"
                  >
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                  </Control.select>
                </Col>
              </Row>
              <Row className="form-group">
                <Col htmlFor="yourname" md={12} className="mb-2">
                  Your Name
                </Col>
                <Col md={12}>
                  <Control.text
                    model=".author"
                    id="author"
                    name="author"
                    placeholder="Your Name"
                    className="form-control"
                    validators={{
                      required,
                      minLength: minLength(3),
                      maxLength: maxLength(15),
                    }}
                  />
                  <Errors
                    className="text-danger"
                    model=".author"
                    show="touched"
                    messages={{
                      required: "Required",
                      minLength: "Must be greater than 2 characters",
                      maxLength: "Must be 15 characters or less",
                    }}
                  />
                </Col>
              </Row>

              <Row className="form-group">
                <Col htmlFor="comment" md={12} className="mb-2">
                  Comment
                </Col>
                <Col md={12}>
                  <Control.textarea
                    model=".comment"
                    id="comment"
                    name="comment"
                    rows="6"
                    className="form-control"
                  />
                </Col>
              </Row>
              <Row className="form-group">
                <Col md={{ size: 12 }}>
                  <Button type="submit" color="primary">
                    Submit
                  </Button>
                </Col>
              </Row>
            </LocalForm>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}
